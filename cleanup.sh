#!/bin/sh
# Cleanup of latex files
patterns='*.aux *.aux *.bcf *.log *.out *.run.xml *.synctex.gz *.toc *.bbl *.fdb_latexmk *.fls *.blg *.nav *.snm *.vrb'
for p in $patterns
do
	find . -name "$p" -delete
done
